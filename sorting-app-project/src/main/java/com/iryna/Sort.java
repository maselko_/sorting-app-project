package com.iryna;

import java.util.Arrays;
import java.util.Scanner;

public class Sort {
    public static void sort(int ... array){
        if (array.length > 10) {
            throw new IllegalArgumentException();
        }
        Arrays.sort(array);
        System.out.print("Sorted array: ");
        for (int elem : array){
            System.out.print(elem + " ");
        }
        System.out.println();
    }
}
