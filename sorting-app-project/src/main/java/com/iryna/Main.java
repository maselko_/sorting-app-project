package com.iryna;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Input array: ");
        Scanner sc = new Scanner(System.in);
        String stringArray = sc.nextLine();
        var array = Arrays.stream(stringArray.split(" ")).mapToInt(Integer::parseInt).toArray();
        Sort.sort(array);
    }
}