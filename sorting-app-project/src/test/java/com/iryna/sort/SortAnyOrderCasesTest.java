package com.iryna.sort;

import com.iryna.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortAnyOrderCasesTest {
    private final int[] array;
    private final int[] expectedArray;

    public SortAnyOrderCasesTest(int[] array, int[] expectedArray) {
        this.array = array;
        this.expectedArray = expectedArray;
    }

    @Test
    public void testSortAnyOrderCases(){
        Sort.sort(array);
        assertArrayEquals(array, expectedArray);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{55, 75, 68, 1}, new int[]{1, 55, 68, 75}},
                {new int[]{1, 9, 6, 5}, new int[]{1, 5, 6, 9}},
                {new int[]{95, 108, 45, 7}, new int[]{7, 45, 95, 108}},
                {new int[]{96, 63, 55, 108}, new int[]{55, 63, 96, 108}}
        });
    }
}
