package com.iryna.sort;
import com.iryna.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortOutSizeLimitCasesTest {
    private final int [] array;

    public SortOutSizeLimitCasesTest(int [] array) {
        this.array = array;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortOutSizeCasesLimit(){
        Sort.sort(array);
    }
    
    @Parameterized.Parameters
    public static Collection<int []> data(){
        return Arrays.asList(
                new int[] {11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1},
                new int[] {11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
                new int[] {12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
                new int[] {12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 13}
        );
    }
}
