package com.iryna.sort;

import com.iryna.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortSortedArrayCasesTest {
    private final int [] array;
    private final int [] expectedArray;

    public SortSortedArrayCasesTest(int[] array, int[] expectedArray) {
        this.array = array;
        this.expectedArray = expectedArray;
    }

    @Test
    public void testSortSortedArrayCases(){
        Sort.sort(array);
        assertArrayEquals(array, expectedArray);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{78, 99}, new int[]{78, 99}},
                {new int[]{65, 66, 67}, new int[]{65, 66, 67}},
                {new int[]{1}, new int[]{1}},
        });
    }
}
