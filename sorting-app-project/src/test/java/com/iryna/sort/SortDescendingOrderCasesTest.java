package com.iryna.sort;

import com.iryna.Sort;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortDescendingOrderCasesTest {
    private final int[] array;
    private final int[] expectedArray;

    public SortDescendingOrderCasesTest(int[] array, int[] expectedArray) {
        this.array = array;
        this.expectedArray = expectedArray;
    }

    @Test
    public void testSortOutSizeCasesLimit() {
        Sort.sort(array);
        assertArrayEquals(array, expectedArray);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{9, 8, 7}, new int[]{7, 8, 9}},
                {new int[]{98, 78, 7}, new int[]{7, 78, 98}},
                {new int[]{55, 33, 7, 2}, new int[]{2, 7, 33, 55}}
        });
    }

}
